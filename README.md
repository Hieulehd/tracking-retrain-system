# Diagram system

<p align="center">
  <img src="system.png">
</p>

- 0: User tương tác với giao diện web, tạo một Session cho user đó. Dựng một API tracking behavior bên phía client.
- 2: Producer gửi event data tới topic predict
- 3: Producer gửi event data tới topic log event
- 4: Consumer dữ liệu từ topic log event và đẩy vào db
- 5: Consumer dữ liệu từ topic predict và gửi tới predicter RS . Ở đây sẽ thực hiện model đã được học offline cho các dự đoán.
- 6: Publish kết quả từ bước 5 tới topic predict
- 1: App/Service sẽ consumming lại dữ liệu đó từ topic predict và hiển thị cho user
- 7: Sau một khoảng thời gian nhất định (cài đặt trigger), publish một message tới topic retrain
- 8: Trainner RS Consumer message từ topic retrain, tiến hành lấy dữ liệu mới thu thập 9 từ mongodb và train lại model mới
- 10: Publish một message tới topic retrain khi model đã được train xong và lưu vào disk
- 11: Predicter RS sẽ consumming message từ topic retrain, load model mới và tiếp tục quá trình predict real time

## Trong quá trình retrain lại model sẽ không làm gián đoạn predicter rs đưa ra các đề xuất cho user
